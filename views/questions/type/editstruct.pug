include ../../meta/datatables

.container-fluid.p-0
    .row#sqHeader.col-grp-header
        .col
            h4.mt-2 Short Structured Question Report Feedback
                span.float-right +
            hr.mt-0.mb-1
    .row.mt-2.collapse.col-grp-1.col-grp-2
        .col
            h5 If Correct Answer
            .form-group
                span.text-muted Leave blank to not show in report
                textarea.form-control.rich#correctFeedback(name='correctFeedback')= structOpts.correctFeedback
        .col
            h5 If Wrong Answer
            .form-group
                span.text-muted Leave blank to not show in report
                textarea.form-control.rich#wrongFeedback(name='wrongFeedback')= structOpts.wrongFeedback
    .row#ansHeader.mt-2.col-grp-header
        .col
            h4 Answers
                span.float-right +
            hr.mt-0.mb-1
    .row.collapse.col-grp-1.col-grp-3
        .col
            h4.required-nonfg#keywordTableColor Answer Keywords
            if clone==true 
                p(style="font-weight: bold;").bold NOTE: You can only add, update or delete the keywords after you have cloned the question.
            else
                span.text-muted These are keywords or phrases to look out for when grading these questions
                br
                span.text-muted If score is blank, the max score will be divided across number of keywords here. Scores will also be rounded off to 1 decimal place
    .row.collapse.col-grp-1.col-grp-3
        .col
            table#keywordTable.table.w-100
            div#keywordTableReq.invalid-feedback You need at least 1 keyword in this section to update this question
            script.
                let kwTable;
                $(document).ready(() => {
                    let kwurl;
                    var clone = #{clone};
                    if(clone==true)
                        kwurl = '/questions/info/#{bankid}/data/clonestructured/#{qnid}';
                    else 
                        kwurl = '/questions/info/#{bankid}/data/structured/#{qnid}';
                    let ktc = $('#keywordTable');
                    if(clone!=true){
                        kwTable = ktc.DataTable({
                        ajax: kwurl,
                        dom: 'Brtilp',
                        responsive: true,
                        altEditor: true,
                        autoWidth: false,
                        deferRender: true,
                        select: 'single',
                        pageLength: 5,
                        lengthMenu: [5, 10, 20, 50, 100],
                        buttons: [
                            {text: 'Add', name: 'add'},
                            {extend: 'selected', text: 'Edit', name: 'edit'},
                            {extend: 'selected', text: 'Delete', name: 'delete'},
                            {text: 'Clear All', className: 'btn-danger', action: () => {
                                if (confirm("Are you sure you want to clear all keywords? THIS ACTION CANNOT BE REVERSED"))
                                    $.ajax({url: kwurl, type: 'DELETE', data: {type: "all"}, success: () => ktc.DataTable().ajax.reload()});
                                }
                            }
                        ],
                        columns: [
                            {title: 'ID', data: 'id', readonly: true, visible: false, type: 'hidden'},
                            {title: "Keyword/Phrase", data: "keyword", type: 'text', className: 'newlinesupport'},
                            {title: "Occurance", data: "occurance", type: 'number'},
                            {title: "Score", data: "score", type: 'number', step: 'any'},
                            {title: "Case Sensitivity", data: "casesensitive", type: 'checkbox'}
                        ],
                        onAddRow: function (datatable, rowdata, success, error) {
                            $.ajax({url: kwurl, type: 'PUT', data: rowdata, success: success, error: error});
                        },
                        onDeleteRow: function (datatable, rowdata, success, error) {
                            $.ajax({url: kwurl, type: 'DELETE', data: rowdata, success: success, error: error});
                        },
                        onEditRow: function (datatable, rowdata, success, error) {
                            $.ajax({url: kwurl, type: 'POST', data: rowdata, success: success, error: error});
                        }
                    });
                    }else{
                        kwTable = ktc.DataTable({
                        ajax: kwurl,
                        dom: 'Brtilp',
                        responsive: true,
                        altEditor: true,
                        autoWidth: false,
                        deferRender: true,
                        select: 'single',
                        pageLength: 5,
                        lengthMenu: [5, 10, 20, 50, 100],
                        columns: [
                            {title: 'ID', data: 'id', readonly: true, visible: false, type: 'hidden'},
                            {title: "Keyword/Phrase", data: "keyword", type: 'text', className: 'newlinesupport'},
                            {title: "Occurance", data: "occurance", type: 'number'},
                            {title: "Score", data: "score", type: 'number', step: 'any'},
                            {title: "Case Sensitivity", data: "casesensitive", type: 'checkbox'}
                        ]
                    });
                    }
                    
                });

script.
    $(document).on("alteditor:delete_dialog_opened", () => {
        let modal_selector = $('#keywordTable')[0].altEditor.modal_selector;
        let ic = $(modal_selector).find("#Keyword\\/Phrase").closest('div');
        $("<span>Are you sure you wish to remove this keyword? This cannot be reversed!</span><br /><br />").insertBefore(ic);
        $(modal_selector).find("#Occurance").closest('div').removeText();
        $(modal_selector).find("label[for=occurance]").remove();
        $(modal_selector).find("label[for=score]").remove();
        $(modal_selector).find("label[for=casesensitive]").remove();
        $(modal_selector).find("#Case\\ Sensitivity").closest('div').remove();
        $(modal_selector).find("#Score").closest('div').removeText();
    });
script.
    $('#sqHeader').click(() => {$(".col-grp-1").collapse('hide'); $(".col-grp-2").collapse('show');$('.collapse-btn-adv').show();updateExp('#sqHeader',true);});
    $('#ansHeader').click(() => {$(".col-grp-1").collapse('hide');$(".col-grp-3").collapse('show');$('.collapse-btn-adv').show();updateExp('#ansHeader',true);});
script.
    function innerFormValidation(result) {
        let rowCnt = kwTable.data().count();
        let kwNotFound = !rowCnt || rowCnt < 0;
        if (kwNotFound) {
            $("#keywordTableReq").show();
            $("#keywordTableColor").addClass("text-danger");
            return false;
        } else {
            $("#keywordTableReq").hide();
            $("#keywordTableColor").removeClass("text-danger");
        }
        return result;
    }