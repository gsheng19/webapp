const redis = require('redis');
const {redisConfig, rateLimitConfig} = require('../config');
const redisClient = redis.createClient({password: redisConfig.password, host: redisConfig.host, port: redisConfig.port});
const util = require("util");
const {RateLimiterRedis} = require('rate-limiter-flexible');
const log = require('../lib/logger');

redisClient.on('error', (err) => {
    log.error(`[REDIS] Err: ${err}`);
});

if (redisConfig.montiorMode) {
    redisClient.monitor(() => {log.info("Enabled Redis Monitoring");});
    redisClient.on("monitor", (time, args) => { console.log(`[REDIS] ${time}: ${args}`); });
}

let rng = new Date().getTime();
redisClient.set("testconn"+rng, "test connection");
redisClient.get("testconn"+rng, (err, reply) => {
    log.info(reply);
});
redisClient.del("testconn"+rng);

const rateLimit = new RateLimiterRedis({
    storeClient: redisClient,
    keyPrefix: 'ratelimit',
    points: rateLimitConfig.request,
    duration: rateLimitConfig.period,
    blockDuration: rateLimitConfig.block
});

const rateLimitCode = new RateLimiterRedis({
    storeClient: redisClient,
    keyPrefix: 'ratelimit-compiler',
    points: rateLimitConfig.request,
    duration: rateLimitConfig.period,
    blockDuration: rateLimitConfig.block
});

const rateLimiterMiddle = (req, res, next) => {
    rateLimit.consume(req.ip).then(() => {next();}).catch(() => {res.status(429).send("Too Many Requests")});
}

const redisInstance = {};
redisInstance.client = redisClient;
redisInstance.config = redisConfig;
redisInstance.get = util.promisify(redisClient.get).bind(redisClient);
redisInstance.set = util.promisify(redisClient.set).bind(redisClient);
redisInstance.keys = util.promisify(redisClient.keys).bind(redisClient);
redisInstance.hmset = util.promisify(redisClient.hmset).bind(redisClient);
redisInstance.hgetall = util.promisify(redisClient.hgetall).bind(redisClient);
redisInstance.rpush = util.promisify(redisClient.rpush).bind(redisClient);
redisInstance.lrange = util.promisify(redisClient.lrange).bind(redisClient);
redisInstance.del = util.promisify(redisClient.del).bind(redisClient);
redisInstance.ttl = util.promisify(redisClient.ttl).bind(redisClient);
redisInstance.rateLimit = rateLimit;
redisInstance.compilerateLimit = rateLimitCode;
redisInstance.setDefault = async (key, value) => { await redisInstance.set(key, value, 'EX', 10800);}
redisInstance.rateLimitGenericMiddleware = rateLimiterMiddle;

module.exports = redisInstance;