const logging = require('pino')({
    prettyPrint: {
        translateTime: true,
        ignore: 'pid,hostname'
    }
});
logging.info("Initialized Logger");

function getEnv() {
    return process.env.NODE_ENV || "development";
}

if (getEnv() === 'development') logging.level = "debug";

module.exports.info = logging.info.bind(logging);
module.exports.error = logging.error.bind(logging);
module.exports.warn = logging.warn.bind(logging);
module.exports.debug = logging.debug.bind(logging);