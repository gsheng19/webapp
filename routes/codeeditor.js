const express = require('express');
const router = express.Router();
const {miscConfigs} = require('../config')
const log = require('../lib/logger');

const auth = require('../lib/auth');

// Test MySQL and Redis working
const db = require('../lib/db');
const cache = require('../lib/redis');
const path = require('path');
const appUtil = require('../lib/util');

const renderConf = {appName: 'AASP'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// View test editor
router.get('/', async (req, res) => {
  let [codeEditors] = await db.query('SELECT name, templatekey FROM code_language WHERE iscodelang=1');
  let [otherEditors] = await db.query('SELECT name, templatekey FROM code_language WHERE iscodelang=0');
  res.render('testeditor', {...renderConf, title: 'Test Code Editor', codeedit: codeEditors, otheredit: otherEditors});
});

// Obtain code editor configuration
router.get('/config/:language', async (req, res) => {
  let language = req.params.language;
  let codeLang = (await db.query('SELECT * FROM code_language WHERE templatekey=?', [language]))[0][0];
  let temp = language;
  if (codeLang.alternatekey) temp = codeLang.alternatekey;
  let data;
  if (req.query.qid) { // Take template from db instead
    let qnid = req.query.qid;
    let codeLangObj = (await db.query('SELECT type, template, filename, lockedlines FROM question_code_template WHERE questionid=? AND type=?', [qnid, language]))[0][0];
    data = codeLangObj.template;
    codeLang.locked = codeLangObj.lockedlines;
    if (codeLangObj.filename) {
      codeLang.defaultFilename = codeLangObj.filename;
      codeLang.hasNamedName = true;
    } else
      codeLang.hasNamedName = false;
  } else {
    let templateLoc = path.join(__dirname, '..', 'codelang', `${language}.txt`);
    data = await appUtil.readFile(templateLoc, 'utf8');
  }
  log.debug(codeLang);
  res.json({template: data, type: temp, editLang: codeLang });
});

// Compile practice code
router.post('/compile/practice', async (req, res) => {
  cache.compilerateLimit.consume(req.ip).then(async () => {
    let keygen = "code-p-";

    // Generate code type in JSON
    let data = {};
    data.status = "Waiting for Compiler";
    data.completed = false;
    data.input = {language: req.body.language, stdin: req.body.input, files: [{name: req.body.filename, content: req.body.code}]};

    log.debug(data);

    // Make sure the key will be unique
    let uniqueKey = new Date().getTime();
    uniqueKey += appUtil.generateRandomChar(8);
    keygen += uniqueKey;

    // Add to redis and push to compiler (later)
    await cache.set(keygen, JSON.stringify(data), 'EX', 10800); // Save to redis for 3 hours
    await cache.rpush(miscConfigs.codeQueue, JSON.stringify({key: uniqueKey, type: "practice"}));

    res.json({success: true, output: keygen});
  }).catch(() => {
    res.json({success: false, output: "Too many compilation requests. Please wait a while before trying again", status: "Rate Limited"});
  });

});

// Compile code with tests for a question
router.post('/compile/tests/:qid', async (req, res) => {
  cache.compilerateLimit.consume(req.ip).then(async () => {
    let keygen = "code-t-";

    // Get tests
    let qid = req.params.qid;
    let opt = (await db.query('SELECT testfullsuite, genericerrors FROM question_code WHERE questionid=?', [qid]))[0][0];

    let tests;
    if (opt.testfullsuite === 1) [tests] = await db.query('SELECT * FROM question_code_tests WHERE questionid=?', [qid]);
    else [tests] = await db.query('SELECT * FROM question_code_tests WHERE questionid=? AND isactualcase=0', [qid]);

    if (tests.length <= 0) {
      res.json({success: false, output: 'No tests', status: 'Error'});
      return;
    }

    let testResult = [];
    for (let t of tests) {
      if (!t.score) t.score = 0
      testResult.push({expectedOutput: (t.isactualcase === 1) ? "-" : t.output, actualOutput: "", state: 0, score: 0, status: "Waiting for Compiler"});
    }

    // Generate code type in JSON
    let data = {};
    data.status = "Waiting for Compiler";
    data.completed = false;
    data.tests = testResult;
    data.isgenericerror = (opt.genericerrors === 1)
    data.input = {language: req.body.language, stdin: req.body.input, files: [{name: req.body.filename, content: req.body.code}]};

    log.debug(data);

    // Make sure the key will be unique
    let uniqueKey = new Date().getTime();
    uniqueKey += appUtil.generateRandomChar(8);
    keygen += uniqueKey;

    // Add to redis and push to compiler (later)
    await cache.set(keygen, JSON.stringify(data), 'EX', 10800); // Save to redis for 3 hours
    await cache.set(keygen + "-tests", JSON.stringify(tests), 'EX', 10800); // Save to redis for 3 hours
    await cache.rpush(miscConfigs.codeQueue, JSON.stringify({key: uniqueKey, type: "tests"}));

    res.json({success: true, output: keygen});
  }).catch(() => {
    res.json({success: false, output: "Too many compilation requests. Please wait a while before trying again", status: "Rate Limited"});
  })

});

// Get progress of compilation
router.get('/progress/:key', async (req, res) => {
  let result = await cache.get(req.params.key);
  if (!result) result = { status: "Invalid Key", completed: true, result: { stdout: "", stderr: "", error: "" } }
  else result = JSON.parse(result);
  if (result.completed) await cache.compilerateLimit.reward(req.ip); // Remove count
  res.json(result);
});

module.exports = router;
