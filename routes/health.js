const express = require('express');
const router = express.Router();

// Check health page
router.get('/', (req, res) => {
  res.status(200).end();
});

module.exports = router;
