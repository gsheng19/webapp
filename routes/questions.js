const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const appUtil = require('../lib/util');
const fs = require('fs');
const log = require('../lib/logger');

const db = require('../lib/db');
const cache = require('../lib/redis');
const unzipper = require('unzipper');

const renderConf = {appName: 'Question Bank Management - AASP', route: 'Questions'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// Add new question bank page
router.get('/add', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    let [users] = await db.query("SELECT u.id, u.username, u.fullname FROM users u JOIN roles r on r.id = u.role WHERE r.power >= (SELECT p.reqpower FROM permissions p WHERE p.key='managequestions')");
    let failmsg;
    if (req.query.res === 'f') failmsg = 'An error occurred creating this question bank';
    res.render('questions/addbank', {...renderConf, title: 'Add Questions', users: users, accessList: [renderConf.loginId], errmsg: failmsg});
});

// Create new question bank
router.post('/add', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');

    let accessList = [];
    if (req.body.accessList) {
        if (Array.isArray(req.body.accessList)) accessList=req.body.accessList;
        else accessList.push(req.body.accessList);
    }

    let dbConn = await db.getConnection();
    try {
        // Create new question bank
        await dbConn.beginTransaction();
        let insertBankSQL = 'INSERT INTO questionsbank(name, topic, purpose, creator) VALUES ?';
        let [result] = await dbConn.query(insertBankSQL, [[[req.body.name, req.body.topic, req.body.purpose, renderConf.loginId]]]);
        let id = result.insertId;

        // Add access rights to question bank
        let accessOpts = [];
        for (let i of accessList) accessOpts.push([id, i]);
        await dbConn.query('INSERT INTO questionbank_access(bankid, userid) VALUES ?', [accessOpts]); // Grant access
        await dbConn.commit();
        res.redirect(`/questions/list?res=s`);
    } catch (err) {
        log.error(err);
        await dbConn.rollback();
        res.redirect(`/questions/add?res=f`);
    } finally {
        await dbConn.release();
    }
});

// List question banks
router.get('/list', async (req, res) => {
    let failmsg, smsg;
    if (req.query.res) {
        let bankname = "";
        if (req.query.resid) {
            let qid = [req.query.resid];
            bankname = (await db.query("SELECT name FROM questionsbank WHERE id=?", qid))[0][0];
            bankname = bankname.name;
        }
        switch (req.query.res) {
            case 'noperm': failmsg = 'You do not have permission to edit that question bank'; break;
            case 's': smsg = 'Question Bank successfully created!'; break;
            case 'f': failmsg = 'Failed to create question bank!'; break;
            case 'fail': failmsg = `Failed to edit question bank  (${bankname})`; break;
            case 'suc': smsg = `Successfully edited question bank  (${bankname})!`; break;
            case 'faild': failmsg = `Failed to delete question bank (${bankname})`; break;
            case 'sucd': smsg = `Successfully deleted question bank (${bankname})!`; break;
            default: failmsg = 'Unknown Action'; break;
        }
    }
    let isadminLink = (req.query.admin && req.query.admin === 'true');
    let isadmin = (isadminLink) ? "/questions/bankdataadm" : "/questions/bankdata";
    let newrc = {...renderConf};
    if (isadminLink) newrc.route = 'Admin';
    res.render('questions/listbank', {...renderConf, title: 'List Qustion Banks', errmsg: failmsg, sucmsg: smsg, dataLink: isadmin, adminMode: isadminLink});
});

// Get data of all question banks managed by the user
router.get('/bankdata', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    let [questionBank] = await db.query('SELECT * FROM questionsbank WHERE id IN (SELECT bankid FROM questionbank_access WHERE userid = ?) AND deleted=0', [renderConf.loginId]);

    let data = [];
    for (let bank of questionBank) { data.push({id: bank.id, name: bank.name, topic: bank.topic, purpose: bank.purpose}); }
    res.json({data: data});
});

router.get('/bankdataadm', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'manageallquestions');
    let [questionBank] = await db.query('SELECT * FROM questionsbank WHERE deleted=0');

    let data = [];
    for (let bank of questionBank) { data.push({id: bank.id, name: bank.name, topic: bank.topic, purpose: bank.purpose}); }
    res.json({data: data});
});

async function canModifyBank(res, bankid) {
    if (await auth.hasPermissionBool(renderConf, 'manageallquestions')) return true;
    let [chk] = await db.query('SELECT * FROM questionbank_access WHERE userid=? AND bankid=?', [renderConf.loginId, bankid]);
    let chk2 = (await db.query('SELECT * FROM questionsbank WHERE id=? AND deleted=0', [bankid]))[0][0];
    if (!chk || chk.length <= 0 || !chk2) { res.redirect(`/questions/list?res=noperm`); return false; } // If no permission to touch this question bank or it is "deleted"
    return true;
}

async function canModifyQuestion(res, questionid) {
    if (await auth.hasPermissionBool(renderConf, 'manageallquestions')) return true;
    let [chk] = await db.query('SELECT * FROM question_access WHERE userid=? AND questionid=?', [renderConf.loginId, questionid]);
    let chk2 = (await db.query('SELECT * FROM question WHERE id=? AND deleted=0', [questionid]));
    if (!chk || chk.length <= 0 || !chk2) { return false; } // If no permission to touch this question or it is "deleted"
    return true;
}

// Edit a question bank page
router.get('/edit/:bankid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions', 'manageallquestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let getQuestionBankSQL = 'SELECT * FROM questionsbank WHERE id=? AND deleted=0';
    let bank = (await db.query(getQuestionBankSQL, [req.params.bankid]))[0][0];
    let [users] = await db.query("SELECT u.id, u.username, u.fullname FROM users u JOIN roles r on r.id = u.role WHERE r.power >= (SELECT p.reqpower FROM permissions p WHERE p.key='managequestions')");
    let getAccessListSQL = 'SELECT userid FROM questionbank_access WHERE bankid = ?';
    let [accesslist] = await db.query(getAccessListSQL, [req.params.bankid]);
    let accessListIds = []
    for (let al of accesslist) accessListIds.push(al.userid);
    res.render('questions/editbank', {...renderConf, data: bank, title: `Edit ${bank.name}`, users: users, accessList: accessListIds});
});

// Edit question bank
router.post('/edit/:bankid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;

    let accessList = []
    if (req.body.accessList) {
        if (Array.isArray(req.body.accessList)) accessList = req.body.accessList;
        else accessList.push(req.body.accessList);
    }

    let accessOpts = [];
    for (let i of accessList) accessOpts.push([req.params.bankid, i]);

    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        let updateQuestionBankSQL = 'UPDATE questionsbank SET name=?, topic=?, purpose=? WHERE id=?';
        await dbConn.query(updateQuestionBankSQL, [req.body.name, req.body.topic, req.body.purpose, req.params.bankid]);
        let deleteAccessSQL = 'DELETE FROM questionbank_access WHERE bankid=?';
        await dbConn.query(deleteAccessSQL, [req.params.bankid]);
        await dbConn.query('INSERT INTO questionbank_access(bankid, userid) VALUES ?', [accessOpts]);
        await dbConn.commit();
        res.redirect(`/questions/list?res=suc&resid=${req.params.bankid}`);
    } catch (err) {
        await dbConn.rollback();
        log.error(err);
        res.redirect(`/questions/list?res=fail&resid=${req.params.bankid}`);
    } finally {
        await dbConn.release();
    }
});

// Delete a question bank
router.get('/delete/:bankid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    try {
        let deleteBankSQL = 'UPDATE questionsbank SET deleted=1 WHERE id=?';
        await db.query(deleteBankSQL, [req.params.bankid]);
        res.redirect(`/questions/list?res=sucd&resid=${req.params.bankid}`);
    } catch (err) {
        log.error(err);
        res.redirect(`/questions/list?res=faild&resid=${req.params.bankid}`);
    }
});

// Get list of questions in question bank page
router.get('/info/:bankid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let banksql = 'SELECT * FROM questionsbank WHERE id=?';
    let bankinfo = (await db.query(banksql, req.params.bankid))[0][0];
    let errmsg, sucmsg;
    if (req.query.act) {
        switch (req.query.act) {
            case 'faivq': errmsg = 'Adding this type of question is coming soon!'; break;
            case 'su': sucmsg = 'Question added to question bank successfully!'; break;
            case 'sea': sucmsg = 'Question updated successfully!'; break;
            case 'sead': sucmsg = 'Question deleted successfully!'; break;
            case 'sum': sucmsg = 'Question moved successfully!'; break;
            case 'fam': errmsg = 'Failed to reorder question'; break;
            case 'fae': errmsg = 'Question not found'; break;
            case 'fea': errmsg = 'An error occurred editing this question'; break;
            case 'fead': errmsg = 'An error occurred deleting this question'; break;
            case 'fa': errmsg = 'An error occurred adding this question'; break;
            case 'na': errmsg = 'Not authorized to modify this question'; break;
        }
    } else if (req.query.imps) {
        let data = await cache.get(req.query.imps);
        data = JSON.parse(data);
        if (data.errors.length > 0) errmsg = `Failed to import ${data.errors.length} questions!`;
        else sucmsg = 'Questions imported successfully!';
    }
    res.render('questions/listquestions', {...renderConf, title: bankinfo.name, data: bankinfo, errmsg: errmsg, sucmsg: sucmsg});
});

// Retrieve data of the list of questions in a question bank
router.get('/info/:bankid/data', async (req, res) => {
    if (req.params.bankid === "-1") { res.json({data: {}}); return; } // Empty dataset
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnSQL = 'SELECT * FROM question WHERE questionbankid=? AND deleted=0';
    let [questions] = await db.query(qnSQL, req.params.bankid);
    let data = [];
    for (let qn of questions) {
        let typeStr, diff;
        switch (qn.type) {
            case 1: typeStr = 'Multiple-Choice'; break;
            case 2: typeStr = 'Short Structured'; break;
            case 3:
                typeStr = 'Coding';
                log.info("questionid = "+qn.id);
                let diffSQL = (await db.query("SELECT difficulty FROM question_code WHERE questionid=?", [qn.id]))[0][0];
                switch (diffSQL.difficulty) {
                    case 0: diff = "Easy"; break;
                    case 1: diff = "Medium"; break;
                    case 2: diff = "Hard"; break;
                }
                break;
            case 4: typeStr = 'Mathematical'; break;
            default: typeStr = 'Unknown'; break;
        }
        data.push({id: qn.id, typeInt: qn.type, type: typeStr, question: qn.question, name: qn.name, score: qn.maxscore, pos: qn.pos, size: questions.length, difficulty: diff});
    }
    
    data.sort((a, b) => (a.pos > b.pos) ? 1 : -1); // Reorganize quiz list according to position
    let pos = 1;
    let hasReordered = false;
    data.forEach((d) => {
        if (d.pos !== pos) {
            d.pos = pos;
            hasReordered = true;
        }
        pos++;
    });
    log.debug('Reordered: %s', hasReordered);
    if (hasReordered) {
        log.warn("Error with quiz order, Updating order list in DB");
        setNewOrder(data).then(r => log.debug("Update Done")); // Ignore async as this should run in background
    }

    res.json({data: data})
});

async function setNewOrder(data) {
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        for (let d of data) { await dbConn.query('UPDATE question SET pos=? WHERE id=?', [d.pos, d.id]); }
        await dbConn.commit();
        log.info("Updated quiz order for failed order question list");
    } catch (err) {
        await dbConn.rollback();
        log.warn("Error updating quiz order. Failing gracefully");
    } finally {
        await dbConn.release();
    }
}

// Add a new question to question bank
router.get('/info/:bankid/add/:type', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let extradata = {};
    extradata.qntypeCaps = req.params.type.charAt(0).toUpperCase() + req.params.type.slice(1);
    extradata.bankid = req.params.bankid;
    extradata.qntype = req.params.type;
    switch (req.params.type) {
        case 'code':
            let [languages] = await db.query('SELECT name, templatekey FROM code_language WHERE iscodelang=1');
            extradata.languages = languages;
            break;
        case 'structured':
            break;
        case 'mcq':
            break;
        default: res.redirect(`/questions/info/${req.params.bankid}?act=faivq`); return;
    }
    res.render('questions/addqns', {...renderConf, title: 'Add Questions', editorFPRaw: `question/${req.params.bankid}/`, bankid: req.params.bankid, ...extradata });
});

async function addCases(dbConn, id, sampleIn, sampleOut, sampleCPU, sampleMem, actualIn, actualOut, actualScore, actualCPU, actualMem) {
    // Process sample and actual test cases
    let sample = [];
    if (Array.isArray(sampleIn) && Array.isArray(sampleOut)) {
        for (let [i] of sampleIn.entries()) { sample.push([id,sampleIn[i],sampleOut[i],0,sampleCPU[i],sampleMem[i]]); }
    } else sample.push([id,sampleIn,sampleOut,0,sampleCPU,sampleMem]);

    let actual = [];
    if (Array.isArray(actualIn) && Array.isArray(actualOut)) {
        for (let [i] of actualIn.entries()) {
            let sc = 1;
            if (actualScore[i] !== '') sc = parseFloat(actualScore[i]);
            actual.push([id,actualIn[i],actualOut[i],sc,1,actualCPU[i],actualMem[i]]);
        }
    } else {
        let sc = 1;
        if (actualScore !== '') sc = parseFloat(actualScore);
        actual.push([id,actualIn,actualOut,sc,1,actualCPU,actualMem]);
    }

    // Add to Code Sample and Actual Test Cases
    if (sample.length > 0) await dbConn.query('INSERT INTO question_code_tests(questionid, input, output, isactualcase, cpulimit, memlimit) VALUES ?', [sample]);
    if (actual.length > 0) await dbConn.query('INSERT INTO question_code_tests(questionid, input, output, score, isactualcase, cpulimit, memlimit) VALUES ?', [actual]);
}

// Handle data regarding adding code test cases. We will need to pull user id out)
// Get test cases in question
router.get('/info/:bankid/data/code/:type/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `codeqnlang-${renderConf.loginId}-${req.params.type}-${req.params.qid}`;

    // Get data
    let data = await getSubData(key);

    res.json({data: data});
});

// Clone test cases in question
router.get('/info/:bankid/data/clonecode/:type/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `codeqnlang-${renderConf.loginId}-${req.params.type}--1`;
    // Get data
    let data = await getSubData(key);

    res.json({data: data});
});

async function addTestCaseToCache(key, dataInRaw, paramtype) {
    // Get data
    let [data, id] = await getSubId(key);

    let dataInArray = []; // Add in as array
    if (!Array.isArray(dataInRaw)) dataInArray.push(dataInRaw);
    else dataInArray = dataInRaw;

    let obj;
    for (let dataIn of dataInArray) {
        if (dataIn.caseScore) dataIn.caseScore = (Math.round((parseFloat(dataIn.caseScore) + Number.EPSILON) * 10) / 10) + "";
        obj = {id: id, inputCases: dataIn.inputCases, outputCases: dataIn.outputCases, cpulimit: dataIn.cpulimit, memlimit: dataIn.memlimit};
        id++;
        if (paramtype === 'atc') obj.caseScore = dataIn.caseScore;
        data.push(obj);
    }
    await cache.set(key, JSON.stringify(data), 'EX', 10800);
    return obj;
}

// Add test cases to question
router.put('/info/:bankid/data/code/:type/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    let clm = req.body.cpulimit, mlm = req.body.memlimit;
    if (clm !== '' && parseInt(clm) < 1) req.body.cpulimit = '';
    if (mlm !== '' && parseInt(mlm) < 1) req.body.memlimit = '';
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `codeqnlang-${renderConf.loginId}-${req.params.type}-${req.params.qid}`;
    let obj = await addTestCaseToCache(key, req.body, req.params.type);

    res.json(obj);
});

// Delete test cases from question
router.delete('/info/:bankid/data/code/:type/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `codeqnlang-${renderConf.loginId}-${req.params.type}-${req.params.qid}`;

    // Check if clear all, if so clear all
    if (req.body.type && req.body.type === 'all') {
        await cache.set(key, '', 'EX', 2);
        res.json({success: true});
        return;
    }

    // Get data
    let data = await getSubData(key);
    let obj = {id: parseInt(req.body.id), inputCases: req.body.inputCases, outputCases: req.body.outputCases};
    if (req.params.type === 'atc') obj.caseScore = req.body.caseScore;

    let rem = -1;
    for (let [i,d] of data.entries()) {
        if (d.id === obj.id) {
            rem = i;
            break;
        }
    }
    if (rem !== -1) data.splice(rem, 1);
    await cache.set(key, JSON.stringify(data), 'EX', 10800);

    res.json(obj);
});

// Edit test cases in question
router.post('/info/:bankid/data/code/:type/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `codeqnlang-${renderConf.loginId}-${req.params.type}-${req.params.qid}`;

    // Get data
    let data = await getSubData(key);
    if (req.body.caseScore) req.body.caseScore = (Math.round((parseFloat(req.body.caseScore) + Number.EPSILON) * 10) / 10) + "";
    let obj = {id: parseInt(req.body.id), inputCases: req.body.inputCases, outputCases: req.body.outputCases, cpulimit: req.body.cpulimit, memlimit: req.body.memlimit};
    if (req.params.type === 'atc') obj.caseScore = req.body.caseScore;
    let rem = -1;
    for (let [i,d] of data.entries()) {
        if (d.id === obj.id) {
            rem = i;
            break;
        }
    }
    if (rem !== -1) data[rem] = obj;
    await cache.set(key, JSON.stringify(data), 'EX', 10800);

    res.json(obj);
});

// Upload test cases to question
router.post('/info/:bankid/data/code/:type/:qid/upload', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `codeqnlang-${renderConf.loginId}-${req.params.type}-${req.params.qid}`;

    let form = await appUtil.formidablePromise(req);

    if (!form.files.zipfile) {
        res.json({success: false, msg: "No such file exists", code: 1});
        return;
    }

    if (form.files.zipfile.type !== 'application/zip') {
        res.json({success: false, msg: "Not a zip file", code: 2});
        return;
    }

    // Unzip files
    let toImport = {};
    let zip = fs.createReadStream(form.files.zipfile.path).pipe(unzipper.Parse({forceStream: true}));
    for await (let entry of zip) {
        const fileName = entry.path;
        const type = entry.type; // 'Directory' or 'File'
        if (type === 'Directory') {
            entry.autodrain(); // Don't care any directories
        } else if (fileName.toLowerCase().startsWith('in')) {
            // Input
            let fn = fileName.split('.').slice(0, -1).join('.')
            fn = fn.substring(2);
            if (!toImport[fn]) toImport[fn] = {input: null, option: null, output: null};
            toImport[fn].input = (await entry.buffer()).toString();
        } else if (fileName.toLowerCase().startsWith('out')) {
            // Output
            let fn = fileName.split('.').slice(0, -1).join('.')
            fn = fn.substring(3);
            if (!toImport[fn]) toImport[fn] = {input: null, option: null, output: null};
            toImport[fn].output = (await entry.buffer()).toString();
        } else if (fileName.toLowerCase().startsWith('opt')) {
            // Option
            let fn = fileName.split('.').slice(0, -1).join('.')
            fn = fn.substring(3);
            if (!toImport[fn]) toImport[fn] = {input: null, option: null, output: null};
            toImport[fn].option = (await entry.buffer()).toString();
        } else {
            entry.autodrain();
        }
    }

    for (let i in toImport) {
        let d = toImport[i];
        if (!d.input) d.input = '';
        if (!d.output) {
            if (req.params.type === 'atc') continue; // Skip if ATC as we need output
            d.output = '';
        }
        let sendin = {inputCases: d.input, outputCases: d.output};
        if (d.option) {
            let opt = d.option.split(",").map(item => item.trim());
            if (req.params.type === 'atc') {
                // First value is opt
                sendin.caseScore = opt[0];
                sendin.cpulimit = opt[1];
                sendin.memlimit = opt[2];
            } else {
                sendin.cpulimit = opt[0];
                sendin.memlimit = opt[1];
            }
        } else if (req.params.type === 'atc') {
            sendin.caseScore = "";
        }
        if (!sendin.cpulimit) sendin.cpulimit = '';
        if (!sendin.memlimit) sendin.memlimit = '';
        log.debug(sendin);
        await addTestCaseToCache(key, sendin, req.params.type);
    }

    res.json({success: true, code: 1});
});

// Add new code-based question to question bank
router.post('/info/:bankid/add/code', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let bankid = req.params.bankid;
    let [latestpos] = await db.query('SELECT pos FROM question WHERE questionbankid=? ORDER BY pos DESC LIMIT 1', bankid);
    if (!latestpos[0] || !latestpos[0].pos) latestpos = 1;
    else latestpos = latestpos[0].pos + 1;
    log.info(`Latest Position in DB for Bank ${req.params.bankid}: ${latestpos}`);

    // Get sample test cases and actual test cases out
    let stcKey = `codeqnlang-${renderConf.loginId}-stc--1`;
    let atcKey = `codeqnlang-${renderConf.loginId}-atc--1`;
    let sampleCases = await cache.get(stcKey);
    let actualCases = await cache.get(atcKey);
    let si = [], so = [], ai = [], ao = [], as = [], sc = [], sm = [], ac = [], am = [];
    if (sampleCases) {
        sampleCases = JSON.parse(sampleCases);
        for (let s of sampleCases) {
            si.push(s.inputCases);
            so.push(s.outputCases);
            sc.push((s.cpulimit) ? s.cpulimit : -1);
            sm.push((s.memlimit) ? s.memlimit : -1);
        }
    }
    if (actualCases) {
        actualCases = JSON.parse(actualCases);
        for (let a of actualCases) {
            ai.push(a.inputCases);
            ao.push(a.outputCases);
            as.push(a.caseScore);
            ac.push((a.cpulimit) ? a.cpulimit : -1);
            am.push((a.memlimit) ? a.memlimit : -1);
        }
    }

    // Add to DB
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();

        // Add to General Qn table relevant fields
        let insertQnSQL = 'INSERT INTO question(questionbankid, name, question, maxscore, submission_limit, type, pos) VALUES ?';
        let [result] = await dbConn.query(insertQnSQL, [[[bankid, req.body.qnName, req.body.qn, parseFloat(req.body.maxScore), req.body.qnCount, 3, latestpos]]]);
        let id = result.insertId;

        // Add to Code options
        await dbConn.query('INSERT INTO question_code(questionid, autocomplete, manualcomplete, testfullsuite, genericerrors, difficulty) VALUES ?',
            [[[id, (req.body.autoComp) ? 1 : 0, (req.body.manComp) ? 1 : 0, (req.body.fullTestAllow) ? 1 : 0, (req.body.genericErr) ? 1 : 0, req.body.difficulty]]]);

        // Add to Code templates
        let lockedlines = await appUtil.generateLockedLines(req.body.primaryCode);
        let templateSQL = 'INSERT INTO question_code_template(questionid, type, template, isprimary, filename, lockedlines) VALUES ?';
        await dbConn.query(templateSQL, [[[id, req.body.primaryLang, req.body.primaryCode, 1, req.body.filename, lockedlines]]]);

        // Process sample and actual test cases
        await addCases(dbConn, id, si, so, sc, sm, ai, ao, as, ac, am);
        await cache.set(atcKey, '', 'EX', 2);
        await cache.set(stcKey, '', 'EX', 2);

        await dbConn.commit();
        // update question access list
        let latestqnsid = (await db.query("SELECT id FROM question ORDER BY id DESC LIMIT 1"))[0][0];
        latestqnsid = latestqnsid.id;
        log.info("latest qns id: "+ latestqnsid);
        await dbConn.query("INSERT INTO question_access (questionid, userid) VALUES ?", [[[latestqnsid, renderConf.loginId]]]);


        res.redirect(`/questions/info/${req.params.bankid}?act=su`);
    } catch (e) {
        log.info(e);
        await dbConn.rollback();
        res.redirect(`/questions/info/${req.params.bankid}?act=fa`);
    } finally {
        await dbConn.release();
    }

});

// Reorder questions in a question bank
router.get('/info/:bankid/reorder/:questionid/:from/:to', async (req, res) => {
    log.info(`Moving Question ID ${req.params.questionid} of Bank ID ${req.params.bankid} from pos ${req.params.from} to ${req.params.to}`);
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        let dataVal = [req.params.bankid, req.params.from]
        await db.query('UPDATE question SET pos=pos-1 WHERE questionbankid=? AND pos > ?', dataVal); // Move from above current down 1 pos
        dataVal = [req.params.bankid, req.params.to]
        await db.query('UPDATE question SET pos=pos+1 WHERE questionbankid=? AND pos >= ?', dataVal); // Move from new up 1 pos
        dataVal = [req.params.to, req.params.bankid, req.params.questionid];
        await db.query('UPDATE question SET pos=? WHERE questionbankid=? AND id=?', dataVal); // Set new pos
        await dbConn.commit();
        res.redirect(`/questions/info/${req.params.bankid}?act=sum`);
    } catch (err) {
        log.error(err);
        await dbConn.rollback();
        res.redirect(`/questions/info/${req.params.bankid}?act=fam`);
    } finally {
        await dbConn.release();
    }
});

// Edit a question in a question bank
router.get('/info/:bankid/edit/:questionid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyQuestion(res, req.params.questionid, false))) {res.redirect(`/questions/info/${req.params.bankid}?act=na`); return; } // Kick out of editing mode
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qdata = [req.params.questionid];
    let questionData = (await db.query("SELECT * FROM question WHERE id=? AND deleted=0", qdata))[0][0];
    let isredirect = 'bank';
    if (req.query.red) isredirect = req.query.red;
    if (!questionData) {
        res.redirect(`/questions/info/${req.params.bankid}?act=fae`);
        return;
    }
    let extradata = {};
    extradata.data = questionData;
    extradata.bankid = req.params.bankid;
    extradata.qnid = req.params.questionid;
    extradata.clone = false;
    switch (questionData.type) {
        case 1:
            extradata.qntypeCaps = 'MCQ';
            extradata.qntype = 'mcq';
            let [choiceList] = await db.query("SELECT * FROM question_mcq_ans WHERE questionid=?", qdata)
            let mcqKey = `mcqChoices-${renderConf.loginId}-${req.params.questionid}`;
            await cache.set(mcqKey, '', 'EX', 1); // Clear cache first JIC
            let choiceListArr = [];
            for (let [i, c] of choiceList.entries()) { // controls how data is displayed in edit page
                choiceListArr.push({id: i+1, answer: c.answer, iscorrect: (c.iscorrect === 1), rationale: c.rationale, score: c.score});
            }
            await cache.set(mcqKey, JSON.stringify(choiceListArr), 'EX', 10800);
            extradata.mcqOpts = (await db.query("SELECT * FROM question_mcq WHERE questionid=?", qdata))[0][0];
            break;
        case 2:
            extradata.qntypeCaps = 'Structured';
            extradata.qntype = 'structured';
            let [keywordList] = await db.query("SELECT * FROM question_structured_keywords WHERE questionid=?", qdata)
            let keywordKey = `structqnkw-${renderConf.loginId}-${req.params.questionid}`;
            await cache.set(keywordKey, '', 'EX', 1); // Clear cache first JIC
            let keywordListArr = [];
            for (let [i, kw] of keywordList.entries()) {
                keywordListArr.push({id: i+1, keyword: kw.keyword, occurance: kw.occurance, score: kw.score, casesensitive: (kw.casesensitive === 1)});
            }
            await cache.set(keywordKey, JSON.stringify(keywordListArr), 'EX', 10800);
            extradata.structOpts = (await db.query("SELECT * FROM question_structured WHERE questionid=?", qdata))[0][0];
            break;
        case 3:
            extradata.qntypeCaps = 'Code';
            extradata.qntype = 'code';
            extradata.codeqnOpts = (await db.query("SELECT * FROM question_code WHERE questionid=?", qdata))[0][0];
            extradata.codeTemplate = (await db.query("SELECT * FROM question_code_template WHERE questionid=? AND isprimary=1", qdata))[0][0];
            let [codetestSam] = await db.query("SELECT * FROM question_code_tests WHERE questionid=? AND isactualcase=0", qdata)
            let [codetestAct] = await db.query("SELECT * FROM question_code_tests WHERE questionid=? AND isactualcase=1", qdata)
            let stcKey = `codeqnlang-${renderConf.loginId}-stc-${req.params.questionid}`;
            let atcKey = `codeqnlang-${renderConf.loginId}-atc-${req.params.questionid}`;
            await cache.set(stcKey, '', 'EX', 1);
            await cache.set(atcKey, '', 'EX', 1); // Clear cache first JIC
            let codeTestSamArr = [], codeTestActArr = [];
            for (let cts of codetestSam) codeTestSamArr.push({inputCases: cts.input, outputCases: cts.output, cpulimit: (cts.cpulimit === -1) ? '' : cts.cpulimit,
                memlimit: (cts.memlimit === -1) ? '' : cts.memlimit});
            for (let ats of codetestAct) codeTestActArr.push({inputCases: ats.input, outputCases: ats.output, caseScore: ats.score,
                cpulimit: (ats.cpulimit === -1) ? '' : ats.cpulimit, memlimit: (ats.memlimit === -1) ? '' : ats.memlimit});
            if (codeTestSamArr.length > 0) await addTestCaseToCache(stcKey, codeTestSamArr, 'stc');
            if (codeTestActArr.length > 0) await addTestCaseToCache(atcKey, codeTestActArr, 'atc');

            let [languages] = await db.query('SELECT name, templatekey, alternatekey FROM code_language WHERE iscodelang=1');
            if (extradata.codeqnOpts.cpulimit === -1) extradata.codeqnOpts.cpulimit = ''
            if (extradata.codeqnOpts.memlimit === -1) extradata.codeqnOpts.memlimit = ''
            extradata.languages = languages;
            extradata.languagesStr = JSON.stringify(languages);
            break;
        default: res.redirect(`/questions/info/${req.params.bankid}?act=faivq`); return;
    }
    // check question access list
    if (!(await canModifyQuestion(res, req.params.questionid, false))) {res.redirect(`/questions/info/${req.params.bankid}`); return; } // Kick out of editing mode
    res.render('questions/editqns', {...renderConf, title: 'Edit Question', editorFPRaw: `question/${req.params.bankid}/`, isred: isredirect, bankid: req.params.bankid, qnid: req.params.questionid, ...extradata });
});

// Clone a question in a question bank
router.get('/info/:bankid/clone/:questionid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) { res.redirect(`/questions/info/${req.params.bankid}`); return; }
    let qdata = [req.params.questionid];
    let questionData = (await db.query("SELECT * FROM question WHERE id=? AND deleted=0", qdata))[0][0];
    let isredirect = 'bank';
    if (req.query.red) isredirect = req.query.red;
    if (!questionData) {
        res.redirect(`/questions/info/${req.params.bankid}?act=fae`);
        return;
    }
    let extradata = {};
    extradata.data = questionData;
    extradata.bankid = req.params.bankid;
    extradata.qnid = req.params.questionid;
    
    let latestqnsid = (await db.query("SELECT id FROM question ORDER BY id DESC LIMIT 1"))[0][0];
    latestqnsid = latestqnsid.id;
    qid = extradata.qnid;
    extradata.clone = true;
    switch (questionData.type) {
        case 1:
            extradata.qntypeCaps = 'MCQ';
            extradata.qntype = 'mcq';
            let [choiceList] = await db.query("SELECT * FROM question_mcq_ans WHERE questionid=?", qdata)

            let mcqKey = `mcqChoices-${renderConf.loginId}--1`; //clone as new key
            await cache.set(mcqKey, '', 'EX', 1); // Clear cache first JIC
            let choiceListArr = [];
            for (let [i, c] of choiceList.entries()) { // controls how data is displayed in edit page
                log.info("..."+c.answer);
                choiceListArr.push({id: i+1, answer: c.answer, iscorrect: (c.iscorrect === 1), rationale: c.rationale, score: c.score});
            }
            await cache.set(mcqKey, JSON.stringify(choiceListArr), 'EX', 10800);
            extradata.mcqOpts = (await db.query("SELECT * FROM question_mcq WHERE questionid=?", qdata))[0][0];
            break;
        case 2:
            extradata.qntypeCaps = 'Structured';
            extradata.qntype = 'structured';
            let [keywordList] = await db.query("SELECT * FROM question_structured_keywords WHERE questionid=?", qdata)
            let keywordKey = `structqnkw-${renderConf.loginId}--1`;  //clone as new key
            await cache.set(keywordKey, '', 'EX', 1); // Clear cache first JIC
            let keywordListArr = [];
            for (let [i, kw] of keywordList.entries()) {
                keywordListArr.push({id: i+1, keyword: kw.keyword, occurance: kw.occurance, score: kw.score, casesensitive: (kw.casesensitive === 1)});
            }
            await cache.set(keywordKey, JSON.stringify(keywordListArr), 'EX', 10800);
            extradata.structOpts = (await db.query("SELECT * FROM question_structured WHERE questionid=?", qdata))[0][0];
            break;
        case 3:
            extradata.qntypeCaps = 'Code';
            extradata.qntype = 'code';
            extradata.codeqnOpts = (await db.query("SELECT * FROM question_code WHERE questionid=?", qdata))[0][0];
            extradata.codeTemplate = (await db.query("SELECT * FROM question_code_template WHERE questionid=? AND isprimary=1", qdata))[0][0];
            let [codetestSam] = await db.query("SELECT * FROM question_code_tests WHERE questionid=? AND isactualcase=0", qdata)
            let [codetestAct] = await db.query("SELECT * FROM question_code_tests WHERE questionid=? AND isactualcase=1", qdata)
            let stcKey = `codeqnlang-${renderConf.loginId}-stc--1`;  //clone as new key
            let atcKey = `codeqnlang-${renderConf.loginId}-atc--1`;
            await cache.set(stcKey, '', 'EX', 1);
            await cache.set(atcKey, '', 'EX', 1); 
            let codeTestSamArr = [], codeTestActArr = [];
            for (let cts of codetestSam) codeTestSamArr.push({inputCases: cts.input, outputCases: cts.output, cpulimit: (cts.cpulimit === -1) ? '' : cts.cpulimit,
                memlimit: (cts.memlimit === -1) ? '' : cts.memlimit});
            for (let ats of codetestAct) codeTestActArr.push({inputCases: ats.input, outputCases: ats.output, caseScore: ats.score,
                cpulimit: (ats.cpulimit === -1) ? '' : ats.cpulimit, memlimit: (ats.memlimit === -1) ? '' : ats.memlimit});
            if (codeTestSamArr.length > 0) await addTestCaseToCache(stcKey, codeTestSamArr, 'stc');
            if (codeTestActArr.length > 0) await addTestCaseToCache(atcKey, codeTestActArr, 'atc');

            let [languages] = await db.query('SELECT name, templatekey, alternatekey FROM code_language WHERE iscodelang=1');
            if (extradata.codeqnOpts.cpulimit === -1) extradata.codeqnOpts.cpulimit = ''
            if (extradata.codeqnOpts.memlimit === -1) extradata.codeqnOpts.memlimit = ''
            extradata.languages = languages;
            extradata.languagesStr = JSON.stringify(languages);
            break;
        default: res.redirect(`/questions/info/${req.params.bankid}?act=faivq`); return;
    }
    res.render('questions/cloneqns', {...renderConf, title: 'Clone Question', editorFPRaw: `question/${req.params.bankid}/`, isred: isredirect, bankid: req.params.bankid, qnid: req.params.questionid, ...extradata });
});

// Update code questions in a question bank
// noinspection DuplicatedCode
router.post('/info/:bankid/edit/:questionid/3', async (req, res) => {
    // Code (type=3)
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;

    let bankid = req.params.bankid;
    let qid = req.params.questionid;
    let redirectBase = `/questions/info/${bankid}`;
    if (req.body.redirect === 'qninfo') redirectBase = `/questions/info/${bankid}/qninfo/${qid}`;
    let redirectAction = (req.body.redirect === 'qninfo') ? 'res' : 'act';

    // Get sample test cases and actual test cases out
    let stcKey = `codeqnlang-${renderConf.loginId}-stc-${qid}`;
    let atcKey = `codeqnlang-${renderConf.loginId}-atc-${qid}`;
    let sampleCases = await cache.get(stcKey);
    let si = [], so = [], ai = [], ao = [], as = [], sc = [], sm = [], ac = [], am = [];
    if (sampleCases) {
        sampleCases = JSON.parse(sampleCases);
        for (let s of sampleCases) {
            si.push(s.inputCases);
            so.push(s.outputCases);
            sc.push((s.cpulimit) ? s.cpulimit : -1);
            sm.push((s.memlimit) ? s.memlimit : -1);
        }
    }
    let actualCases = await cache.get(atcKey);
    if (actualCases) {
        actualCases = JSON.parse(actualCases);
        for (let a of actualCases) {
            ai.push(a.inputCases);
            ao.push(a.outputCases);
            as.push(a.caseScore);
            ac.push((a.cpulimit) ? a.cpulimit : -1);
            am.push((a.memlimit) ? a.memlimit : -1);
        }
    }

    // Update DB
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();

        // Update question
        let updateSQL = "UPDATE question SET name=?, question=?, maxscore=?, submission_limit = ? WHERE id=?";
        await dbConn.query(updateSQL, [req.body.qnName, req.body.qn, req.body.maxScore, req.body.qnCount, qid]);
        // Update Code Question Options
        updateSQL = "UPDATE question_code SET autocomplete=?, manualcomplete=?, testfullsuite=?, genericerrors=?, difficulty=? WHERE questionid=?";
        await dbConn.query(updateSQL, [(req.body.autoComp) ? 1 : 0, (req.body.manComp) ? 1 : 0, (req.body.fullTestAllow) ? 1 : 0, (req.body.genericErr) ? 1 : 0, req.body.difficulty, qid]);
        // Update Primary template
        let lockedlines = await appUtil.generateLockedLines(req.body.primaryCode);
        updateSQL = "UPDATE question_code_template SET type=?, template=?, filename=?, lockedlines=? WHERE questionid=? AND isprimary=1";
        await dbConn.query(updateSQL, [req.body.primaryLang, req.body.primaryCode, req.body.filename, lockedlines, qid]);
        // Update test cases
        await dbConn.query("DELETE FROM question_code_tests WHERE questionid=?", [qid])
        // Process sample and actual test cases
        await addCases(dbConn, qid, si, so, sc, sm, ai, ao, as, ac, am);
        await cache.set(atcKey, '', 'EX', 2);
        await cache.set(stcKey, '', 'EX', 2);

        await dbConn.commit();
        res.redirect(`${redirectBase}?${redirectAction}=sea`);
    } catch (err) {
        log.error(err);
        await dbConn.rollback();
        res.redirect(`${redirectBase}?${redirectAction}=fea`);
    } finally {
        await dbConn.release();
    }
});

// Update short structured questions in a question bank
router.post('/info/:bankid/edit/:questionid/2', async (req, res) => {
    // Short Structured (type=2)
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;

    let bankid = req.params.bankid;
    let qid = req.params.questionid;
    let redirectBase = `/questions/info/${bankid}`;
    if (req.body.redirect === 'qninfo') redirectBase = `/questions/info/${bankid}/qninfo/${qid}`;
    let redirectAction = (req.body.redirect === 'qninfo') ? 'res' : 'act';

    let keywordKey = `structqnkw-${renderConf.loginId}-${qid}`;
    let keywords = await cache.get(keywordKey);
    let kw = [], ko = [], ks = [], kcs = [];
    if (keywords) {
        keywords = JSON.parse(keywords);
        for (let k of keywords) {
            kw.push(k.keyword);
            ko.push(k.occurance);
            ks.push(k.score);
            kcs.push((k.casesensitive) ? 1 : 0);
        }
    }
    // Update DB
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        // Update Question Table
        let updateSQL = "UPDATE question SET name=?, question=?, maxscore=?, submission_limit = ? WHERE id=?";
        await dbConn.query(updateSQL, [req.body.qnName, req.body.qn, req.body.maxScore, req.body.qnCount, qid]);

        // Update structured options
        updateSQL = "UPDATE question_structured SET correctFeedback = ?, wrongFeedback = ? WHERE questionid=?";
        await dbConn.query(updateSQL, [(req.body.correctFeedback === '') ? null : req.body.correctFeedback, (req.body.wrongFeedback === '') ? null : req.body.wrongFeedback, qid]);

        // Update Keywordss
        await dbConn.query("DELETE FROM question_structured_keywords WHERE questionid=?", [qid])
        await addKeywords(dbConn, qid, kw, ko, ks, kcs); // Add keywords
        await cache.set(keywordKey, '', 'EX', 2);
        await dbConn.commit();
        res.redirect(`${redirectBase}?${redirectAction}=sea`);
    } catch (e) {
        log.error(e);
        await dbConn.rollback();
        res.redirect(`${redirectBase}?${redirectAction}=fea`);
    } finally {
        await dbConn.release();
    }
});

// Update multiple choice questions (mcq) in a question bank
router.post('/info/:bankid/edit/:questionid/1', async (req, res) => {
    // MCQ (type=1)
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;

    let bankid = req.params.bankid;
    let qid = req.params.questionid;
    let redirectBase = `/questions/info/${bankid}`;
    if (req.body.redirect === 'qninfo') redirectBase = `/questions/info/${bankid}/qninfo/${qid}`;
    let redirectAction = (req.body.redirect === 'qninfo') ? 'res' : 'act';

    let mcqKey = `mcqChoices-${renderConf.loginId}-${qid}`;
    let choices = await cache.get(mcqKey);
    let ans = [], corr = [], rat = [], sco = [];
    if (choices) {
        choices = JSON.parse(choices);
        for (let k of choices) {
            ans.push(k.answer);
            corr.push(k.iscorrect);
            rat.push(k.rationale);
            sco.push(k.score);
        }
    }
    // Update DB
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        // Update Question Table
        let updateSQL = "UPDATE question SET name=?, question=?, maxscore=?, submission_limit = ? WHERE id=?";
        await dbConn.query(updateSQL, [req.body.qnName, req.body.qn, req.body.maxScore, req.body.qnCount, qid]);

        // Update MCQ configurations
        updateSQL = "UPDATE question_mcq SET randomanswers = ?, style = ? WHERE questionid=?";
        await dbConn.query(updateSQL, [(req.body.randomanswers === 'on') ? 1 : 0, (req.body.style === 'on') ? 1 : 0, qid]);

        // Update MCQ Choices
        await dbConn.query("DELETE FROM question_mcq_ans WHERE questionid=?", [qid])
        await addMCQChoices(dbConn, qid, ans, corr, rat, sco); // Add choices
        await cache.set(mcqKey, '', 'EX', 2);
        await dbConn.commit();
        res.redirect(`${redirectBase}?${redirectAction}=sea`);
    } catch (e) {
        log.error(e);
        await dbConn.rollback();
        res.redirect(`${redirectBase}?${redirectAction}=fea`);
    } finally {
        await dbConn.release();
    }
});


// Delete question from question bank
router.get('/info/:bankid/delete/:qnid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    try {
        let deleteBankSQL = 'UPDATE question SET deleted=1 WHERE id=? AND questionbankid=?';
        await db.query(deleteBankSQL, [req.params.qnid, req.params.bankid]);
        res.redirect(`/questions/info/${req.params.bankid}?act=sead`);
    } catch (err) {
        log.error(err);
        res.redirect(`/questions/info/${req.params.bankid}?act=fead`);
    }
});

// Export questions page (select questions to export)
router.get('/info/:bankid/export', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let banksql = 'SELECT * FROM questionsbank WHERE id=?';
    let bankinfo = (await db.query(banksql, req.params.bankid))[0][0];
    res.render('questions/exportquestions', {...renderConf, title: bankinfo.name, data: bankinfo});
});

// Generate JSON of questions that are being exported
router.post('/info/:bankid/export', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let inid = req.body['qid[]'];
    if (!Array.isArray(inid)) inid = [inid];
    let outT = []
    for (let i of inid) {
        // Get all the data about this question
        let data = (await db.query('SELECT * FROM question WHERE id =?', [i]))[0][0];
        let qn = {name: data.name, question: data.question, maxscore: data.maxscore };
        switch (data.type) {
            case 3:
                qn.type = "code";
                // Get all coding question related data
                let codeopt = (await db.query('SELECT * FROM question_code WHERE questionid=?', [i]))[0][0];
                let [codeTemp] = await db.query('SELECT * FROM question_code_template WHERE questionid=?', [i]);
                let [codeTestsSamp] = await db.query('SELECT * FROM question_code_tests WHERE questionid=? AND isactualcase=0', [i]);
                let [codeTestsAct] = await db.query('SELECT * FROM question_code_tests WHERE questionid=? AND isactualcase=1', [i]);
                qn.codeOpt = {autocomplete: codeopt.autocomplete, manualcomplete: codeopt.manualcomplete, testfullsuite: codeopt.testfullsuite,
                    genericerrors: codeopt.genericerrors, difficulty: codeopt.difficulty};
                let template = [];
                for (let ct of codeTemp) {template.push({type: ct.type, template: ct.template, primary: ct.isprimary, filename: ct.filename});}
                qn.codeTemplate = template;
                let testSamp = [], testAct = [];
                for (let cts of codeTestsSamp) {testSamp.push({input: cts.input, output: cts.output, cpulimit: cts.cpulimit, memlimit: cts.memlimit});}
                for (let cta of codeTestsAct) {testAct.push({input: cta.input, output: cta.output, score: cta.score, cpulimit: cta.cpulimit, memlimit: cta.memlimit});}
                qn.codeTests = {sample: testSamp, actual: testAct};
                break;
            case 2:
                qn.type = "structured";
                // Get all structured related data
                let structopt = (await db.query('SELECT * FROM question_structured WHERE questionid=?', [i]))[0][0];
                let [structkeywords] = await db.query('SELECT * FROM question_structured_keywords WHERE questionid=?', [i]);
                qn.structOpt = {correctFeedback: structopt.correctFeedback, wrongFeedback: structopt.wrongFeedback};
                let keywords = [];
                for (let sk of structkeywords) keywords.push({keyword: sk.keyword, occurance: sk.occurance, score: sk.score, casesensitive: sk.casesensitive});
                qn.structKeywords = keywords;
                break;
            case 1:
                 qn.type = "mcq"; 
                // Get all MCQ related data
                let mcqopt = (await db.query('SELECT * FROM question_mcq WHERE questionid=?', [i]))[0][0];
                let [mcqChoices] = await db.query('SELECT * FROM question_mcq_ans WHERE questionid=?', [i]);
                qn.mcqOpt = {randomanswers: mcqopt.randomanswers, style: mcqopt.style};
                let MCQtemplate = [];
                for (let c of mcqChoices) MCQtemplate.push({answer: c.answer, iscorrect: c.iscorrect, rationale: c.rationale,score: c.score});
                qn.MCQTests = MCQtemplate;
                break;
            default:
                qn.type = "unknown";
                break;
        }
        outT.push(qn);
    }
    res.json({questions: outT});
});

// Import questions to question bank page
router.get('/info/:bankid/import', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let banksql = 'SELECT * FROM questionsbank WHERE id=?';
    let bankinfo = (await db.query(banksql, req.params.bankid))[0][0];
    res.render('questions/importquestions', {...renderConf, title: bankinfo.name, data: bankinfo});
});

// Import JSON of questions into question bank
router.post('/info/:bankid/import', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let banksql = 'SELECT * FROM questionsbank WHERE id=?';
    let bankinfo = (await db.query(banksql, req.params.bankid))[0][0];

    try {
        let form = await appUtil.formidablePromise(req);
        let jsonFile = form.files.importqn;
        let json = JSON.parse(await fs.readFileSync(jsonFile.path));
        if (!json.questions || json.questions.length <=0) {
            res.render('questions/importquestions', {...renderConf, title: bankinfo.name, data: bankinfo, errmsg: "Invalid or Empty JSON file"});
            return;
        }
        json.bankid = bankinfo.id;
        // Save to cache temporarily
        let rediskey = `importqn:${new Date().getTime()}:${auth.getSalt()}`;
        await cache.set(rediskey, JSON.stringify(json), 'EX', 10800); // Set redis for 3 hours
        res.render('questions/importquestionscheck', {...renderConf, title: 'Confirm Import Questions', dataCache: rediskey, data: bankinfo, qns: json.questions, qnlen: json.questions.length});
    } catch (err) {
        res.render('questions/importquestions', {...renderConf, title: bankinfo.name, data: bankinfo, errmsg: "An error occurred uploading file"});
    }
});

async function addQnJson(qnJson, bankid) {
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();

        let [latestpos] = await db.query('SELECT pos FROM question WHERE questionbankid=? ORDER BY pos DESC LIMIT 1', bankid);
        if (!latestpos[0] || !latestpos[0].pos) latestpos = 1;
        else latestpos = latestpos[0].pos + 1;

        let qtype = 0;
        switch (qnJson.type) {
            case 'mcq': qtype = 1; break;
            case 'structured': qtype = 2; break;
            case 'code': qtype = 3; break;
        }

        // Add to General Qn table relevant fields
        let [result] = await dbConn.query("INSERT INTO question(questionbankid, name, question, maxscore, type, pos) VALUES ?", [[[bankid, qnJson.name, qnJson.question, parseFloat(qnJson.maxscore), qtype, latestpos]]]);
        let id = result.insertId;
        
        switch (qtype) {
            case 1:
                // Add to MCQ options
                await dbConn.query('INSERT INTO question_mcq(questionid, randomanswers, style) VALUES ?',
                    [[[id, qnJson.mcqOpt.randomanswers, qnJson.mcqOpt.style]]]);
                // Add to keywords
                let choices = [];
                for (let choice of qnJson.mcqChoices) choices.push([id, choice.answer, choice.iscorrect, choice.rationale, choice.score]);
                await dbConn.query('INSERT INTO question_mcq_ans(questionid, answer, iscorrect, rationale, score) VALUES ?',
                    [choices]);
                break;
            case 2:
                // Add to Structured options
                await dbConn.query('INSERT INTO question_structured(questionid, correctFeedback, wrongFeedback) VALUES ?',
                    [[[id, qnJson.structOpt.correctFeedback, qnJson.structOpt.wrongFeedback]]]);
                // Add to keywords
                let keywords = [];
                for (let keywd of qnJson.structKeywords) keywords.push([id, keywd.keyword, keywd.occurance, keywd.score, keywd.casesensitive]);
                await dbConn.query('INSERT INTO question_structured_keywords(questionid, keyword, occurance, score, casesensitive) VALUES ?',
                    [keywords]);
                break;
            case 3:
                // Add to Code options
                await dbConn.query('INSERT INTO question_code(questionid, autocomplete, manualcomplete, testfullsuite, genericerrors, difficulty) VALUES ?',
                    [[[id, qnJson.codeOpt.autocomplete, qnJson.codeOpt.manualcomplete, qnJson.codeOpt.testfullsuite, qnJson.codeOpt.genericerrors, qnJson.codeOpt.difficulty]]]);

                // Add to Code templates
                let templates = [];
                for (let t of qnJson.codeTemplate) {
                    templates.push([id, t.type, t.template, t.primary, t.filename]);
                }
                let templateSQL = 'INSERT INTO question_code_template(questionid, type, template, isprimary, filename) VALUES ?';
                await dbConn.query(templateSQL, [templates]);

                // Process sample and actual test cases
                let si = [], so = [], ai = [], ao = [], as = [], scm = [], sm = [], acm = [], am = [];
                for (let sc of qnJson.codeTests.sample) {
                    si.push(sc.input);
                    so.push(sc.output);
                    scm.push(sc.cpulimit);
                    sm.push(sc.memlimit);
                }

                for (let ac of qnJson.codeTests.actual) {
                    ai.push(ac.input);
                    ao.push(ac.output);
                    as.push(ac.score);
                    acm.push(ac.cpulimit);
                    am.push(ac.memlimit);
                }
                await addCases(dbConn, id, si, so, scm, sm, ai, ao, as, acm, am);
                break;
        }

        await dbConn.commit();
        // update question access list
        let latestqnsid = (await db.query("SELECT id FROM question ORDER BY id DESC LIMIT 1"))[0][0];
        latestqnsid = latestqnsid.id;
        log.info("latest qns id: "+ latestqnsid);
        await dbConn.query("INSERT INTO question_access (questionid, userid) VALUES ?", [[[latestqnsid, renderConf.loginId]]]);
    } catch (err) {
        await dbConn.rollback();
        throw err;
    } finally {
        await dbConn.release();
    }
}

async function bulkAddQuestions(redisKey, cachekey) {
    await cache.set(cachekey, JSON.stringify({status: "Getting data from cache", finished: false}), 'EX', 10800);
    let data = await cache.get(redisKey);
    data = JSON.parse(data);
    let id = data.bankid;
    data = data.questions;
    await cache.set(cachekey, JSON.stringify({status: `Preparing to import ${data.length} questions`, finshed: false}), 'EX', 10800);
    let error = 0, cnt = 0;
    let errRow = [];
    for (let d of data) {
        cnt++;
        await cache.set(cachekey, JSON.stringify({status: `Importing ${cnt}/${data.length} questions. Errors: ${error}`, finished: false}), 'EX', 10800);
        try {
            await addQnJson(d, id);
        } catch (err) {
            log.error(err);
            error++;
            errRow.push(d.question);
        }
    }
    await cache.set(cachekey, JSON.stringify({status: `Finished importing ${cnt} questions! There are ${error} errors. Redirecting you back shortly...`, finished: true, errors: errRow}), 'EX', 10800);
}

// Confirm import of questions into question bank
router.post('/info/:bankid/importcfm', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let banksql = 'SELECT * FROM questionsbank WHERE id=?';
    let bankinfo = (await db.query(banksql, req.params.bankid))[0][0];
    let redisKey = req.body.cachekey;
    let newrediskey = `importqnU-${auth.getSalt()}`;
    await cache.set(newrediskey, JSON.stringify({status: "Processing", finished: false}), 'EX', 10800);
    bulkAddQuestions(redisKey, newrediskey).then(() => log.info("Bulk add completed"));
    res.render('questions/processImport', {...renderConf, title: 'Processing Import...', processKey: newrediskey, data: bankinfo});
});

// Check import of questions to question bank status
router.get('/importStatus/:key', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    let info = await cache.get(req.params.key);
    res.json(JSON.parse(info));
});

router.accessQnInfo = async (qnid) => {
    let qdata = await getQuestionInfoFull(qnid);
    if(qdata.type === 3)
        qdata = await appUtil.checkLockedLines(qnid, qdata);
    if (qdata.type === 3) {
        let [codeEditors] = await db.query('SELECT name, templatekey FROM code_language WHERE iscodelang=1');
        qdata.code.editorList = codeEditors;
        let opt = (await db.query('SELECT testfullsuite FROM question_code WHERE questionid=?', [qnid]))[0][0];
        if (opt.testfullsuite === 1) qdata.code.testCount = (await db.query('SELECT COUNT(id) AS cid FROM question_code_tests WHERE questionid=?', [qnid]))[0][0].cid;
        else qdata.code.testCount = (await db.query('SELECT COUNT(id) AS cid FROM question_code_tests WHERE questionid=? AND isactualcase=0', [qnid]))[0][0].cid;
    }
    return qdata;
}

async function getQuestionInfoFull(qnid) {
    let qninfo = (await db.query('SELECT * FROM question WHERE id=?', [qnid]))[0][0];
    let codesamplessize = []; //store code length to be used at view qns (coding) page
    switch (qninfo.type) {
        case 1:
            let mcq = {};
            mcq.config = (await db.query('SELECT questionid, randomanswers, style FROM question_mcq WHERE questionid=?', [qnid]))[0][0];
            mcq.choices = [];
            let [choices] = await db.query('SELECT answer, iscorrect, rationale, score FROM question_mcq_ans WHERE questionid=?', [qnid]);
            for (let c of choices) {
                mcq.choices.push({answer: c.answer, iscorrect: (c.iscorrect === 1), rationale: c.rationale, score: c.score});
            }
            if(mcq.config.randomanswers === 1){ //randomize order if randomanswers is true
                for (var i = mcq.choices.length - 1; i > 0; i--) {
                    var j = Math.floor(Math.random() * (i + 1));
                    var temp = mcq.choices[i];
                    mcq.choices[i] = mcq.choices[j];
                    mcq.choices[j] = temp;
                }
            }
            qninfo.mcq = mcq;
            break;
        case 2:
            let structured = {};
            structured.feedback = (await db.query('SELECT correctFeedback, wrongFeedback FROM question_structured WHERE questionid=?', [qnid]))[0][0];
            structured.keywords = [];
            let [keywords] = await db.query('SELECT keyword, occurance, score, casesensitive FROM question_structured_keywords WHERE questionid=?', [qnid]);
            for (let kw of keywords) {
                structured.keywords.push({keyword: kw.keyword, occurance: kw.occurance, score: kw.score, casesensitive: (kw.casesensitive === 1)});
            }
            qninfo.structured = structured;
            break;
        case 3:
            // Pull code templates, tests and opts
            let code = {};
            code.options = (await db.query('SELECT autocomplete, manualcomplete, testfullsuite, genericerrors, difficulty FROM question_code WHERE questionid=?', [qnid]))[0][0];
            code.options.difficultyName = (code.options.difficulty === 0) ? "Easy" : (code.options.difficulty === 1) ? "Medium" : "Hard";
            let [tests] = await db.query('SELECT input, output, score, isactualcase, cpulimit, memlimit FROM question_code_tests WHERE questionid=?', [qnid]);
            code.samples = [];
            code.actual = [];
            let lowestcpu = -1, lowestmem = -1;
            for (let t of tests) {
                if (t.isactualcase === 1) {
                    code.actual.push({input: t.input, output: t.output, score: t.score, cpulimit: (t.cpulimit === -1) ? "No Limit" : t.cpulimit, memlimit: (t.memlimit === -1) ? "No Limit" : t.memlimit});
                } else {
                    code.samples.push({input: t.input, output: t.output, cpulimit: (t.cpulimit === -1) ? "No Limit" : t.cpulimit, memlimit: (t.memlimit === -1) ? "No Limit" : t.memlimit});
                }
                if (t.cpulimit !== -1 && (lowestcpu === -1 || lowestcpu > t.cpulimit)) lowestcpu = t.cpulimit;
                if (t.memlimit !== -1 && (lowestmem === -1 || lowestmem > t.memlimit)) lowestmem = t.memlimit;
            }
            code.options.cpulimit = (lowestcpu === -1) ? "No Limit" : lowestcpu;
            code.options.memlimit = (lowestmem === -1) ? "No Limit" : lowestmem;
            let [templates] = await db.query('SELECT type, template, isprimary, filename, lockedlines, hidelines FROM question_code_template WHERE questionid=?', [qnid]);
            code.templates = [];
            for (let te of templates) {
                code.templates.push({type: te.type, template: te.template, isprimary: te.isprimary, filename: te.filename, locked: te.lockedlines, hide: te.hidelines});
            }
            codesamplessize = code.samples;
            qninfo.code = code;
            break;
    }
    qninfo.codesamplessize = codesamplessize;
    return qninfo;
}

// Get information about question
router.get('/info/:bankid/qninfo/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let qninfo = await getQuestionInfoFull(qnid);
    let sucmsg, errmsg;
    if (req.query.res) {
        switch (req.query.res) {
            case 's': sucmsg = 'Language added to question successfully!'; break;
            case 'slang': sucmsg = 'Update primary language successfully!'; break;
            case 'sedit': sucmsg = 'Language updated successfully!'; break;
            case 'sdel': sucmsg = 'Language deleted successfully!'; break;
            case 'flang': errmsg = 'Failed to update primary language'; break;
            case 'sea': sucmsg = 'Question updated successfully!'; break;
            case 'fea': errmsg = 'An error occurred editing this question'; break;
        }
    }
    let permToEdit = true;
    if (!(await canModifyQuestion(res, qnid, false))) { permToEdit = false; }
    res.render('questions/qninfo', {...renderConf, title: 'Question Info', data: qninfo, sucmsg: sucmsg, errmsg: errmsg, permToEdit: permToEdit});
});

// Get code templates in a coding question
router.get('/info/:bankid/qninfo/:qid/codetemplates', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let [templates] = await db.query('SELECT type, template, isprimary, filename FROM question_code_template WHERE questionid=?', [qnid]);
    let [languages] = await db.query('SELECT * FROM code_language');
    let langMap = {};
    for (let lan of languages) {
        langMap[lan.templatekey] = lan;
    }
    for (let temp of templates) {
        temp.langname = langMap[temp.type].name;
        temp.isprimarybool = (temp.isprimary === 1);
    }
    res.json({data: templates});
});

// Make a language the primary langauge of a coding question
router.get('/info/:bankid/qninfo/:qid/mkpri/:lang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let newPriLang = req.params.lang;

    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        await dbConn.query("UPDATE question_code_template SET isprimary=0 WHERE questionid=?", [qnid]); // Unset pri lang
        await dbConn.query("UPDATE question_code_template SET isprimary=1 WHERE questionid=? AND type=?", [qnid, newPriLang]); // Set new pri lang
        await dbConn.commit();
        res.redirect(`/questions/info/${req.params.bankid}/qninfo/${qnid}?res=slang`);
    } catch (err) {
        await dbConn.rollback();
        log.error(err);
        res.redirect(`/questions/info/${req.params.bankid}/qninfo/${qnid}?res=flang`);
    } finally {
        await dbConn.release();
    }



    let [templates] = await db.query('SELECT type, template, isprimary FROM question_code_template WHERE questionid=?', [qnid]);
    let [languages] = await db.query('SELECT * FROM code_language');
    let langMap = {};
    for (let lan of languages) {
        langMap[lan.templatekey] = lan;
    }
    for (let temp of templates) {
        temp.langname = langMap[temp.type].name;
        temp.isprimarybool = (temp.isprimary === 1);
    }
    res.json({data: templates});
});

// Display a preview of how the coding question will look in an actual quiz
router.get('/info/:bankid/qninfo/:qid/trycode', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let qninfo = await getQuestionInfoFull(qnid);
    qninfo = await appUtil.checkLockedLines(qnid, qninfo);
    let [codeEditors] = await db.query('SELECT name, templatekey FROM code_language WHERE iscodelang=1');
    // find primary language
    let isprimary = 'java';
    let enabledLangs = [];
    for (let lang of qninfo.code.templates) {
        enabledLangs.push(lang.type);
        if (lang.isprimary) isprimary = lang.type;
    }
    // Get test cases count based on options
    let opt = (await db.query('SELECT testfullsuite FROM question_code WHERE questionid=?', [qnid]))[0][0];
    let tests;
    if (opt.testfullsuite === 1) tests = (await db.query('SELECT COUNT(id) AS cid FROM question_code_tests WHERE questionid=?', [qnid]))[0][0];
    else tests = (await db.query('SELECT COUNT(id) AS cid FROM question_code_tests WHERE questionid=? AND isactualcase=0', [qnid]))[0][0];
    res.render('questions/trycodeqn', {...renderConf, title: 'Question Info', data: qninfo, codeedit: codeEditors, enabledLangs: enabledLangs, prilang: isprimary, testcount: tests.cid});
});

// Add language to a coding question page
router.get('/info/:bankid/qninfo/:qid/addlang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let qninfo = await getQuestionInfoFull(qnid);
    let [languages] = await db.query('SELECT name, templatekey FROM code_language WHERE iscodelang=1');
    let selLanguages = [];
    for (let sl of qninfo.code.templates) {
        selLanguages.push(sl.type);
    }
    log.debug(selLanguages);
    // find primary language
    let isprimary = 'java';
    for (let lang of qninfo.code.templates) {
        if (lang.isprimary) isprimary = lang.type;
    }
    res.render('questions/addlang', {...renderConf, title: 'Add Language', data: qninfo, languages: languages, selLanguages: selLanguages});
});

// Add new coding langauge to a coding question
router.post('/info/:bankid/qninfo/:qid/addlang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    log.debug(req.body);
    let id = req.params.qid;
    // Add to Code templates
    let lockedlines = await appUtil.generateLockedLines(req.body.code);
    let templateSQL = 'INSERT INTO question_code_template(questionid, type, template, isprimary, filename, lockedlines) VALUES ?';
    await db.query(templateSQL, [[[id, req.body.lang, req.body.code, 0, req.body.filename, lockedlines]]]);
    res.redirect(`/questions/info/${req.params.bankid}/qninfo/${id}?res=s`);
});

// Edit a coding language in a coding question page
router.get('/info/:bankid/qninfo/:qid/editlang/:lang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let lang = req.params.lang;
    let language = (await db.query('SELECT type, template, isprimary FROM question_code_template WHERE questionid=? AND type=?', [qnid, lang]))[0][0];
    let langRec = (await db.query('SELECT name FROM code_language WHERE templatekey=?', [lang]))[0][0];
    language.name = langRec.name;
    res.render('questions/editlang', {...renderConf, title: `Edit ${language.name}`, languages: language, bankid: req.params.bankid, qid: qnid, language: lang});
});

// Edit coding language in code question
router.post('/info/:bankid/qninfo/:qid/editlang/:lang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let lang = req.params.lang;
    let lockedlines = await appUtil.generateLockedLines(req.body.code);
    await db.query('UPDATE question_code_template SET template=?, filename=?, lockedlines=? WHERE questionid=? AND type=?', [req.body.code, req.body.filename, lockedlines, qnid, lang]);
    res.redirect(`/questions/info/${req.params.bankid}/qninfo/${qnid}?res=sedit`);
});

// Delete language from a coding question
router.get('/info/:bankid/qninfo/:qid/dellang/:lang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnid = req.params.qid;
    let lang = req.params.lang;
    await db.query('DELETE FROM question_code_template WHERE questionid=? AND type=?', [qnid, lang]);
    res.redirect(`/questions/info/${req.params.bankid}/qninfo/${qnid}?res=sdel`);
});

async function getCommonLanguage(questions) {
    let qid = [];
    for (let qn of questions) {qid.push(qn.id);}
    if (qid.length <= 0) {
        return {languages: null, hasCL: false};
    }
    let langSQL = "SELECT name, templatekey FROM code_language WHERE templatekey IN (SELECT type FROM question_code_template WHERE questionid in (?) GROUP BY `type` HAVING COUNT(type) >= ?)";
    let [languages] = await db.query(langSQL, [qid, qid.length]);
    let data = [];
    for (let lang of languages) {
        data.push({name: lang.name, key: lang.templatekey});
    }
    return {languages: data, hasCL: true};
}

// Get common languages in a question bank
router.get('/info/:bankid/codecommonlang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let qnSQL = 'SELECT id FROM question WHERE questionbankid=? AND deleted=0 AND type=3 AND id IN (?)';
    let [questions] = await db.query(qnSQL, [req.params.bankid, req.query.qnid.split(",")]);
    res.json((await getCommonLanguage(questions)));
});

// Get common languages among questions
router.get('/codecommonlang', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    let qnSQL = 'SELECT id FROM question WHERE deleted=0 AND type=3 AND id IN (?)';
    let [questions] = await db.query(qnSQL, [req.query.qnid.split(",")]);
    res.json((await getCommonLanguage(questions)));
});

// Handle Structured Questions
// Get keywords
router.get('/info/:bankid/data/structured/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `structqnkw-${renderConf.loginId}-${req.params.qid}`;
    // Get data
    let data = await getSubData(key);

    res.json({data: data});
});

// Clone keywords
router.get('/info/:bankid/data/clonestructured/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `structqnkw-${renderConf.loginId}--1` 
    log.info("@@ kw key="+key);
    // Get data
    let data = await getSubData(key);

    res.json({data: data});
});

// Handle MCQ Questions
// Get MCQ Choices
router.get('/info/:bankid/data/mcq/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `mcqChoices-${renderConf.loginId}-${req.params.qid}`;
    // Get data
    let data = await getSubData(key);

    res.json({data: data});
});

// Clone MCQ Choices
router.get('/info/:bankid/data/clonemcq/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `mcqChoices-${renderConf.loginId}--1` 
    // Get data
    let data = await getSubData(key);
    
    res.json({data: data});
});

async function getSubId(key) {
    let data = await cache.get(key);
    let id = 1;
    if (!data) data = [];
    else {
        data = JSON.parse(data);
        if (data.length === 0) id = 1
        else id = data[data.length - 1].id + 1;
    }
    return [data, id];
}

async function getSubData(key) {
    let data = await cache.get(key);
    if (!data) data = [];
    else data = JSON.parse(data);
    return data;
}

// Add keywords to question
router.put('/info/:bankid/data/structured/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    log.debug(req.body);
    let key = `structqnkw-${renderConf.loginId}-${req.params.qid}`;
    let occurance = (!req.body.occurance || parseInt(req.body.occurance) <= 0) ? 1 : parseInt(req.body.occurance);

    let [data, id] = await getSubId(key);

    let obj = {id: id, keyword: req.body.keyword, occurance: occurance, score: parseFloat(req.body.score), casesensitive: (req.body.casesensitive === 'true')};
    data.push(obj);
    await cache.set(key, JSON.stringify(data), 'EX', 10800);
    res.json(obj);
});

// Add mcq choices to question
router.put('/info/:bankid/data/mcq/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    log.debug(req.body);
    let key = `mcqChoices-${renderConf.loginId}-${req.params.qid}`;
    let [data, id] = await getSubId(key);
    let obj = {id: id, answer: req.body.answer, iscorrect: (req.body.iscorrect === 'true'), rationale: req.body.rationale, score: parseFloat(req.body.score)};
    data.push(obj);
    await cache.set(key, JSON.stringify(data), 'EX', 10800);
    res.json(obj);
});

// Delete keywords from question
router.delete('/info/:bankid/data/structured/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `structqnkw-${renderConf.loginId}-${req.params.qid}`;

    // Check if clear all, if so clear all
    if (req.body.type && req.body.type === 'all') {
        await cache.set(key, '', 'EX', 2);
        res.json({success: true});
        return;
    }

    let data = await getSubData(key);
    let delId = parseInt(req.body.id);

    let rem = -1;
    for (let [i,d] of data.entries()) {
        if (d.id === delId) {
            rem = i;
            break;
        }
    }

    if (rem !== -1) data.splice(rem, 1);
    await cache.set(key, JSON.stringify(data), 'EX', 10800);

    res.json({id: delId});
});

// Delete mcq choices from question
router.delete('/info/:bankid/data/mcq/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `mcqChoices-${renderConf.loginId}-${req.params.qid}`;

    // Check if clear all, if so clear all
    if (req.body.type && req.body.type === 'all') {
        await cache.set(key, '', 'EX', 2);
        res.json({success: true});
        return;
    }

    let data = await getSubData(key);
    let delId = parseInt(req.body.id);

    let rem = -1;
    for (let [i,d] of data.entries()) {
        if (d.id === delId) {
            rem = i;
            break;
        }
    }

    if (rem !== -1) data.splice(rem, 1);
    await cache.set(key, JSON.stringify(data), 'EX', 10800);

    res.json({id: delId});
});

// Edit keywords in question
router.post('/info/:bankid/data/structured/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `structqnkw-${renderConf.loginId}-${req.params.qid}`;

    let data = await getSubData(key);
    let occurance = (!req.body.occurance || parseInt(req.body.occurance) <= 0) ? 1 : parseInt(req.body.occurance);
    let obj = {id: parseInt(req.body.id), keyword: req.body.keyword, occurance: occurance, score: parseFloat(req.body.score), casesensitive: (req.body.casesensitive === 'true')};

    let rem = -1;
    for (let [i,d] of data.entries()) {
        if (d.id === obj.id) {
            rem = i;
            break;
        }
    }

    if (rem !== -1) data[rem] = obj;
    await cache.set(key, JSON.stringify(data), 'EX', 10800);

    res.json(obj);
});

// Edit mcq in question
router.post('/info/:bankid/data/mcq/:qid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let key = `mcqChoices-${renderConf.loginId}-${req.params.qid}`;

    let data = await getSubData(key);
    let obj = {id: parseInt(req.body.id), answer: req.body.answer, iscorrect: (req.body.iscorrect==='true'?true:false), rationale: req.body.rationale, score: parseFloat(req.body.score)};

    let rem = -1;
    for (let [i,d] of data.entries()) {
        if (d.id === obj.id) {
            rem = i;
            break;
        }
    }
    log.info("!!!");
    if (rem !== -1) data[rem] = obj;
    await cache.set(key, JSON.stringify(data), 'EX', 10800);

    res.json(obj);
});

async function addKeywords(dbConn, id, keywords, keyOccurance, keyScore, keySensititivity) {
    // Process sample and actual test cases
    let keywordArr = [];
    for (let [i] of keywords.entries()) {
        let ks;
        if (keyScore[i] && keyScore[i] !== null && keyScore[i] !== '') ks = parseFloat(keyScore[i]);
        else ks = null;

        keywordArr.push([id,keywords[i],keyOccurance[i],ks,keySensititivity[i]]);
    }

    // Add to Code Sample and Actual Test Cases
    if (keywordArr.length > 0) await dbConn.query('INSERT INTO question_structured_keywords(questionid, keyword, occurance, score, casesensitive) VALUES ?', [keywordArr]);
}

// Add new short structured question to question bank
router.post('/info/:bankid/add/structured', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let bankid = req.params.bankid;
    let [latestpos] = await db.query('SELECT pos FROM question WHERE questionbankid=? ORDER BY pos DESC LIMIT 1', bankid);
    if (!latestpos[0] || !latestpos[0].pos) latestpos = 1;
    else latestpos = latestpos[0].pos + 1;
    log.info(`Latest Position in DB for Bank ${req.params.bankid}: ${latestpos}`);

    let keywordKey = `structqnkw-${renderConf.loginId}--1`;
    let keywords = await cache.get(keywordKey);
    let kw = [], ko = [], ks = [], kcs = [];
    if (keywords) {
        keywords = JSON.parse(keywords);
        for (let k of keywords) {
            kw.push(k.keyword);
            ko.push(k.occurance);
            ks.push(k.score);
            kcs.push((k.casesensitive) ? 1 : 0);
        }
    }
    log.debug(req.body);
    // Add to DB
    let dbConn = await db.getConnection();

    try {
        await dbConn.beginTransaction();
        // Add to General Qn table relevant fields
        let insertQnSQL = 'INSERT INTO question(questionbankid, name, question, maxscore, submission_limit, type, pos) VALUES ?';
        let [result] = await dbConn.query(insertQnSQL, [[[bankid, req.body.qnName, req.body.qn, parseFloat(req.body.maxScore), req.body.qnCount, 2, latestpos]]]);
        let id = result.insertId;
        log.info([result]);

        // Add to structured options
        await dbConn.query('INSERT INTO question_structured(questionid, correctFeedback, wrongFeedback) VALUES ?',
            [[[id, (req.body.correctFeedback === '') ? null : req.body.correctFeedback, (req.body.wrongFeedback === '') ? null : req.body.wrongFeedback]]]);

        await addKeywords(dbConn, id, kw, ko, ks, kcs); // Add keywords
        await cache.set(keywordKey, '', 'EX', 2);
        await dbConn.commit();
        // update question access list
        let latestqnsid = (await db.query("SELECT id FROM question ORDER BY id DESC LIMIT 1"))[0][0];
        latestqnsid = latestqnsid.id;
        log.info("latest qns id: "+ latestqnsid);
        await dbConn.query("INSERT INTO question_access (questionid, userid) VALUES ?", [[[latestqnsid, renderConf.loginId]]]);

        res.redirect(`/questions/info/${req.params.bankid}?act=su`);
    } catch (e) {
        log.error(e);
        await dbConn.rollback();
        res.redirect(`/questions/info/${req.params.bankid}?act=fa`);
    } finally {
        await dbConn.release();
    }
});

// Add new multiple choice question (mcq) to question bank
router.post('/info/:bankid/add/mcq', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'managequestions');
    if (!(await canModifyBank(res, req.params.bankid))) return;
    let bankid = req.params.bankid;
    let [latestpos] = await db.query('SELECT pos FROM question WHERE questionbankid=? ORDER BY pos DESC LIMIT 1', bankid);
    if (!latestpos[0] || !latestpos[0].pos) latestpos = 1;
    else latestpos = latestpos[0].pos + 1;
    log.info(`Latest Position in DB for Bank ${req.params.bankid}: ${latestpos}`);
    log.info("ADD CODE IS HERE");
    let mcqKey = `mcqChoices-${renderConf.loginId}--1`;
    let choices = await cache.get(mcqKey);
    let ka = [], ki = [], kr = [], ks = [];
    if (choices) {
        choices = JSON.parse(choices);
        for (let c of choices) {
            ka.push(c.answer);
            ki.push((c.iscorrect) ? 1 : 0);
            kr.push(c.rationale);
            ks.push(c.score);
        }
    }
    log.debug(req.body);
    // Add to DB
    let dbConn = await db.getConnection();

    try {
        await dbConn.beginTransaction();
        // Add to General Qn table relevant fields
        let insertQnSQL = 'INSERT INTO question(questionbankid, name, question, maxscore, submission_limit, type, pos) VALUES ?';
        let [result] = await dbConn.query(insertQnSQL, [[[bankid, req.body.qnName, req.body.qn, parseFloat(req.body.maxScore), req.body.qnCount, 1, latestpos]]]); // qns type 1:  mcq
        let id = result.insertId;
        // Add to mcq options
        await dbConn.query('INSERT INTO question_mcq(questionid, randomanswers, style) VALUES ?',
            [[[id, ((req.body.randomanswers === 'on') ? 1 : 0), ((req.body.style === 'on') ? 1 : 0)]]]);
        await addMCQChoices(dbConn, id, ka, ki, kr, ks); // Add mcq choices
        await cache.set(mcqKey, '', 'EX', 2);
        await dbConn.commit();
        // update question access list
        let latestqnsid = (await db.query("SELECT id FROM question ORDER BY id DESC LIMIT 1"))[0][0];
        latestqnsid = latestqnsid.id;
        log.info("latest qns id: "+ latestqnsid);
        await dbConn.query("INSERT INTO question_access (questionid, userid) VALUES ?", [[[latestqnsid, renderConf.loginId]]]);

        res.redirect(`/questions/info/${req.params.bankid}?act=su`);
    } catch (e) {
        log.error(e);
        await dbConn.rollback();
        res.redirect(`/questions/info/${req.params.bankid}?act=fa`);
    } finally {
        await dbConn.release();
    }
});

async function addMCQChoices(dbConn, id, answer, iscorrect, rationale, score) {
   let choiceArr = [];
   for (let [i] of answer.entries()) {
       let cs;
       if (score[i] && score[i] !== null && score[i] !== '') cs = parseFloat(score[i]);
       else cs = 0;
       choiceArr.push([id,answer[i],iscorrect[i],rationale[i],cs]);
   }
   // Add to Code Sample and Actual Test Cases
   if (choiceArr.length > 0) 
        await dbConn.query('INSERT INTO question_mcq_ans(questionid, answer, iscorrect, rationale, score) VALUES ?', [choiceArr]);
}

module.exports = router;
